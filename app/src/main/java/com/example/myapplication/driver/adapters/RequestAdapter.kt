package com.example.myapplication.driver.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.driver.OnRequestClickListener

class RequestAdapter (private var requestid: List<String>, private var requestorname: List<String>, private var datetime: List<String>, private var pickuppoint: List<String>, private var dropoffpoint: List<String>, private var seatnum: List<String>, private val  onRequestClickListener: OnRequestClickListener) :
    RecyclerView.Adapter<RequestAdapter.ViewHolder>(){
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val request_id: String = requestid.toString()
        val requestor_name: TextView = itemView.rootView.findViewById(R.id.tv_requestor)
        val date_time: TextView = itemView.rootView.findViewById(R.id.tv_datetime)
        val pickup_point: TextView = itemView.rootView.findViewById(R.id.tv_pickup_point)
        val dropoff_point: TextView = itemView.rootView.findViewById(R.id.tv_dropoff_point)
        val seat_number: TextView = itemView.rootView.findViewById(R.id.tv_seat_number)


//        init {
//            itemView.setOnClickListener { View ->
//                Toast.makeText(
//                    itemView.context,
//                    "You clicked on item # ${request_id}",
//                    Toast.LENGTH_LONG
//                ).show()
//
//            }
//        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.request_item_layout, p0, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.requestor_name.text = requestorname[p1]
        p0.date_time.text = datetime[p1]
        p0.pickup_point.text = pickuppoint[p1]
        p0.dropoff_point.text = dropoffpoint[p1]
        p0.seat_number.text = seatnum[p1]

        p0.itemView.setOnClickListener{
            onRequestClickListener.onRequestItemClicked(requestid[p1], requestorname[p1], datetime[p1], pickuppoint[p1], dropoffpoint[p1], seatnum[p1])
        }
    }

    override fun getItemCount(): Int {
        return requestorname.size
    }
}