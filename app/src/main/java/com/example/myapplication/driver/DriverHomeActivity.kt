package com.example.myapplication.driver

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.example.myapplication.R
import com.example.myapplication.driver.fragments.DriverHomeFragment
import com.example.myapplication.driver.fragments.DriverProfileFragment
import com.example.myapplication.frontliner.fragments.FrontlinerHomeFragment
import com.example.myapplication.frontliner.fragments.FrontlinerProfileFragment
import com.google.android.material.navigation.NavigationView

class DriverHomeActivity : AppCompatActivity() {

    lateinit var toggle : ActionBarDrawerToggle
    lateinit var drawerLayout: DrawerLayout
    lateinit var navigationView: NavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_driver_home)

        navigationView = findViewById(R.id.driver_nav_view)
        drawerLayout = findViewById(R.id.driverDrawerLayout)
        toggle = ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        replaceFragment(DriverHomeFragment(), "Home")
        navigationView.setNavigationItemSelectedListener {
            when(it.itemId){
                R.id.driver_nav_home -> replaceFragment(DriverHomeFragment(), "Home")
                R.id.driver_nav_profile -> replaceFragment(DriverProfileFragment(), "Profile")
                R.id.driver_nav_schedule -> Toast.makeText(this, "test", Toast.LENGTH_LONG).show()
                R.id.driver_nav_message -> Toast.makeText(this, "test", Toast.LENGTH_LONG).show()
                R.id.driver_nav_history -> Toast.makeText(this, "test", Toast.LENGTH_LONG).show()
                R.id.driver_nav_logout -> Toast.makeText(this, "test", Toast.LENGTH_LONG).show()
            }

            true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(toggle.onOptionsItemSelected(item)){
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun replaceFragment(fragment: Fragment, title: String){
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.driver_framelayout, fragment)
        fragmentTransaction.commit()
        drawerLayout.closeDrawers()
        setTitle(title)
    }
}