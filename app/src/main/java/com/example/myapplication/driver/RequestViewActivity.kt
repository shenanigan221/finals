package com.example.myapplication.driver

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.myapplication.CalsakaySharedPreference
import com.example.myapplication.R
import com.example.myapplication.frontliner.FrontlinerHomeActivity
import org.json.JSONException
import org.json.JSONObject
import org.w3c.dom.Text

class RequestViewActivity : AppCompatActivity() {
    lateinit var request_id: TextView
    lateinit var requestor_name: TextView
    lateinit var date_time: TextView
    lateinit var pickup_point: TextView
    lateinit var dropoff_point: TextView
    lateinit var seat_number: TextView

    lateinit var btn_accept_request: Button
    lateinit var btn_back: Button

    lateinit var requestQueue: RequestQueue
    lateinit var calsakaySharedPreference: CalsakaySharedPreference


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_request_view)

        requestQueue = Volley.newRequestQueue(this)
        calsakaySharedPreference = CalsakaySharedPreference(this)



        request_id = findViewById(R.id.view_request_id)
        requestor_name = findViewById(R.id.view_requestor_name)
        date_time = findViewById(R.id.view_requestor_datetime)
        pickup_point = findViewById(R.id.view_pickup_point)
        dropoff_point = findViewById(R.id.view_dropoff_point)
        seat_number = findViewById(R.id.view_seat_number)

        btn_accept_request = findViewById(R.id.btn_view_accept_request)
        btn_back = findViewById(R.id.btn_view_back)

        request_id.text = intent.getStringExtra("request_id")
        requestor_name.text = intent.getStringExtra("requestor_name")
        date_time.text = intent.getStringExtra("date_time")
        pickup_point.text = intent.getStringExtra("pickup_point")
        dropoff_point.text = intent.getStringExtra("dropoff_point")
        seat_number.text = intent.getStringExtra("seat_number")

        btn_accept_request.setOnClickListener {
            accept_request()
        }

    }

    private fun accept_request(){
        val data = JSONObject()
        val URL = calsakaySharedPreference.getValueString("URL") + "driver/api_accept_request.php"

        data.put("request_id", intent.getStringExtra(""))
        data.put("request_id", calsakaySharedPreference.getValueString("id"))


        val request = JsonObjectRequest(Request.Method.POST, URL, data,{ response -> try {

//            val jsonObject = response.getJSONObject("response")
            Toast.makeText(this, "true", Toast.LENGTH_LONG).show()

        } catch (e: JSONException){
            Log.d("Catch Error: ", e.message.toString())
        }
        },{ error ->
            run {
                error.printStackTrace()
                Log.d("Error", error.message.toString())
            }
        }
        )
        requestQueue.add(request)
    }
}