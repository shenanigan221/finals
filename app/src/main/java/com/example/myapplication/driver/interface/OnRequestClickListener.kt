package com.example.myapplication.driver

interface OnRequestClickListener {
    fun onRequestItemClicked(request_id: String, requestor_name: String, date_time: String, pickup_point: String, dropoff_point: String, seat_number: String)
}