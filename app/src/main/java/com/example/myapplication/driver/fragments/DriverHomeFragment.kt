package com.example.myapplication.driver.fragments

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.myapplication.CalsakaySharedPreference
import com.example.myapplication.R
import com.example.myapplication.driver.OnRequestClickListener
import com.example.myapplication.driver.RequestViewActivity
import com.example.myapplication.driver.adapters.RequestAdapter
import com.google.android.material.bottomsheet.BottomSheetDialog
import org.json.JSONException
import org.json.JSONObject

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [DriverHomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DriverHomeFragment : Fragment(), OnRequestClickListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    private var request_id_list = mutableListOf<String>()
    private var requestor_name_list = mutableListOf<String>()
    private var date_time_list = mutableListOf<String>()
    private var pickup_point_list = mutableListOf<String>()
    private var dropoff_point_list = mutableListOf<String>()
    private var seat_num_list = mutableListOf<String>()

    lateinit var recyclerView: RecyclerView
    lateinit var requestQueue: RequestQueue
    lateinit var calsakaySharedPreference: CalsakaySharedPreference
    lateinit var requestURL: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView =  inflater.inflate(R.layout.fragment_driver_home, container, false)
        recyclerView = rootView.findViewById(R.id.rv_request)
        requestQueue = Volley.newRequestQueue(requireActivity())
        calsakaySharedPreference = CalsakaySharedPreference(requireActivity())
        requestURL = calsakaySharedPreference.getValueString("URL") + "driver/api_get_request.php"



        recyclerView.layoutManager = LinearLayoutManager(context)
        getRequest()
        return rootView
    }
    private fun getRequest(){
    val data = JSONObject()
        val progressDialog = ProgressDialog(requireActivity())
        progressDialog.setMessage("Loading....")
        progressDialog.show()
        val request = JsonObjectRequest(Request.Method.POST, requestURL, data, { response ->
            try {
                val jsonArray = response.getJSONArray("response")
                for(i in 0 until jsonArray.length()){
                    val data = jsonArray.getJSONObject(i)
                    val request_id =data.getString("id")
                    val full_name = data.getString("first_name").toString() + " " + data.getString("last_name").toString()
                    val date_time = data.getString("date_time").toString()
                    val pickup_point = data.getString("pickup_point").toString()
                    val dropoff_point = data.getString("dropoff_point").toString()
                    val seat_number = data.getString("seat_number").toString()

                    addToList(request_id,full_name,"Date: " + date_time, "Pickup point: " + pickup_point, "Drop off: " + dropoff_point, "Seat number: " + seat_number,)
                }
                recyclerView.adapter = RequestAdapter(request_id_list, requestor_name_list, date_time_list, pickup_point_list, dropoff_point_list, seat_num_list, this)
                progressDialog.hide()
            }catch (e: JSONException){
                Toast.makeText(context, "Catch error: " + e.message, Toast.LENGTH_LONG).show()
                Log.d("Catch Error: ", e.message.toString())
                progressDialog.hide()
            }
        },{error ->
            run {
                error.printStackTrace()
                Toast.makeText(context, "Error: " + error.message.toString(), Toast.LENGTH_LONG).show()
                Log.d("Error", error.message.toString())
                progressDialog.hide()
            }
        }
        )
        requestQueue.add(request)
    }

    private fun addToList(id: String, requestor_name: String, date_time: String, pickup_point: String, dropoff_point: String, seat_number: String){
        request_id_list.add(id)
        requestor_name_list.add(requestor_name)
        date_time_list.add(date_time)
        pickup_point_list.add(pickup_point)
        dropoff_point_list.add(dropoff_point)
        seat_num_list.add(seat_number)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment DriverHomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            DriverHomeFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onRequestItemClicked(requestid: String, requestorname: String, datetime: String, pickuppoint: String, dropoffpoint: String, seatnum: String) {
//        Toast.makeText(requireActivity(), "Number" + position.toString(), Toast.LENGTH_LONG).show()
        val intent = Intent(requireActivity(), RequestViewActivity::class.java).apply {
            putExtra("request_id", requestid.toString())
            putExtra("requestor_name", requestorname.toString())
            putExtra("date_time", datetime.toString())
            putExtra("pickup_point", pickuppoint.toString())
            putExtra("dropoff_point", dropoffpoint.toString())
            putExtra("seat_number", seatnum.toString())
        }
        startActivity(intent)
    }
}
