package com.example.myapplication.frontliner

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import com.example.myapplication.CalsakaySharedPreference
import com.example.myapplication.LoginActivity
import com.example.myapplication.R
import com.example.myapplication.frontliner.fragments.FrontlinerHomeFragment
import com.example.myapplication.frontliner.fragments.FrontlinerProfileFragment
import com.google.android.material.navigation.NavigationView

class FrontlinerHomeActivity : AppCompatActivity() {

    lateinit var toggle : ActionBarDrawerToggle
    lateinit var drawerLayout: DrawerLayout
    lateinit var navigationView: NavigationView
    lateinit var calsakaySharedPreference: CalsakaySharedPreference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_frontliner_home)

        calsakaySharedPreference = CalsakaySharedPreference(this)
        navigationView = findViewById(R.id.frontliner_nav_view)
        drawerLayout = findViewById(R.id.frontlinerDrawerLayout)
        toggle = ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close)

        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        replaceFragment(FrontlinerHomeFragment(), "Home")
        navigationView.setNavigationItemSelectedListener {
            when(it.itemId){
                R.id.frontliner_nav_home -> replaceFragment(FrontlinerHomeFragment(), "Home")
                R.id.frontliner_nav_profile -> replaceFragment(FrontlinerProfileFragment(), "Profile")
                R.id.frontliner_nav_schedule -> Toast.makeText(this, "test", Toast.LENGTH_LONG).show()
                R.id.frontliner_nav_message -> Toast.makeText(this, "test", Toast.LENGTH_LONG).show()
                R.id.frontliner_nav_history -> Toast.makeText(this, "test", Toast.LENGTH_LONG).show()
                R.id.frontliner_nav_logout -> {
                    calsakaySharedPreference.clearSharedPreference()
                    val intent = Intent(this, LoginActivity::class.java)
                    startActivity(intent)
                    finish()
                }

            }

            true
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(toggle.onOptionsItemSelected(item)){
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun replaceFragment(fragment: Fragment, title: String){
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frontliner_framelayout, fragment)
        fragmentTransaction.commit()
        drawerLayout.closeDrawers()
        setTitle(title)
    }
}