package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.myapplication.driver.DriverHomeActivity
import com.example.myapplication.frontliner.FrontlinerHomeActivity
import org.json.JSONException
import org.json.JSONObject

class LoginActivity : AppCompatActivity() {

    private lateinit var btnLogin: Button
    private lateinit var btnSignup: Button
    private lateinit var etUsername: EditText
    private lateinit var etPassword: EditText
    private lateinit var calsakaySharedPreference: CalsakaySharedPreference
    private lateinit var requestQueue: RequestQueue


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        requestQueue = Volley.newRequestQueue(this)

        btnLogin = findViewById(R.id.btn_login)
        btnSignup = findViewById(R.id.btn_signup)
        etUsername = findViewById(R.id.editTextUsername)
        etPassword = findViewById(R.id.ediTextPassword)
        calsakaySharedPreference = CalsakaySharedPreference(this)


        btnLogin.setOnClickListener {
            login()
        }


    }

    private fun login(){

        val data = JSONObject()
        val URL = calsakaySharedPreference.getValueString("URL") + "login.php"
        data.put("email", etUsername.text.toString())
        data.put("password", etPassword.text.toString())

        val request = JsonObjectRequest(Request.Method.POST, URL, data,{
                response -> try {

            val jsonObject = response.getJSONObject("response")

            if(jsonObject.getString("response") == "1"){
                if(jsonObject.getString("account_status") == "1"){
                    calsakaySharedPreference.save("hasUser", "1")
                    calsakaySharedPreference.save("user_role", "Medical Frontliner")
                    calsakaySharedPreference.save("id", jsonObject.getString("id"))
                    calsakaySharedPreference.save("first_name", jsonObject.getString("first_name"))
                    calsakaySharedPreference.save("last_name", jsonObject.getString("last_name"))
                    calsakaySharedPreference.save("birthday", jsonObject.getString("birthday"))
                    calsakaySharedPreference.save("gender", jsonObject.getString("gender"))
                    calsakaySharedPreference.save("mobile_number", jsonObject.getString("mobile_number"))
                    calsakaySharedPreference.save("address", jsonObject.getString("address"))
                    calsakaySharedPreference.save("email", jsonObject.getString("email"))
                    calsakaySharedPreference.save("userImage", jsonObject.getString("userImage"))
                    calsakaySharedPreference.save("username", jsonObject.getString("username"))
                    calsakaySharedPreference.save("password", jsonObject.getString("password"))
                    calsakaySharedPreference.save("account_status", jsonObject.getString("account_status"))
                    calsakaySharedPreference.save("role", jsonObject.getString("role"))
                    calsakaySharedPreference.save("front_image_name", jsonObject.getString("front_image_name"))
                    calsakaySharedPreference.save("back_image_name", jsonObject.getString("back_image_name"))

                    if (jsonObject.getString("role") == "1"){
                        Log.d("ROLE", "Frontliner")
                        calsakaySharedPreference.save("company_name", jsonObject.getString("company_name"))
                        calsakaySharedPreference.save("company_address", jsonObject.getString("company_address"))
                        calsakaySharedPreference.save("company_number", jsonObject.getString("company_number"))

                        val intent = Intent(this, FrontlinerHomeActivity::class.java)
                        startActivity(intent)
                        finish()


                    }else if(jsonObject.getString("role") == "2"){
                        Log.d("ROLE", "Driver")
                        calsakaySharedPreference.save("driver_license_front", jsonObject.getString("driver_license_front"))
                        calsakaySharedPreference.save("driver_license_back", jsonObject.getString("driver_license_back"))
                        calsakaySharedPreference.save("orcr_front_image", jsonObject.getString("orcr_front_image"))
                        calsakaySharedPreference.save("vehicle_type", jsonObject.getString("vehicle_type"))
                        calsakaySharedPreference.save("plate_no", jsonObject.getString("plate_no"))
                        calsakaySharedPreference.save("driver_type", jsonObject.getString("driver_type"))

                        val intent = Intent(this, DriverHomeActivity::class.java)
                        startActivity(intent)
                        finish()

                    }else{
                        Toast.makeText(this, "Admin", Toast.LENGTH_LONG).show()
                        Log.d("ROLE", "Admin")
                    }
                }else{
                    Toast.makeText(this, "It looks like your account is not yet activated, please wait for the admin text message for your account verification and try again later", Toast.LENGTH_LONG).show()
                }
            }else if(jsonObject.getString("response") == "0"){
                Toast.makeText(this, "Sorry, we cannot found your account", Toast.LENGTH_LONG).show()
            }else if(jsonObject.getString("response") == "2"){
                Toast.makeText(this, "Something's went wrong, please try to log in later", Toast.LENGTH_LONG).show()
                Log.d("Error", jsonObject.getString("catch_exception"))
            }else{

            }

        } catch (e: JSONException){
            Log.d("Catch Error: ", e.message.toString())
        }
        },{ error ->
            run {
                error.printStackTrace()
                Log.d("Error", error.message.toString())
            }
        }
        )
        requestQueue.add(request)

//        val intent = Intent(this, FrontlinerHomeActivity::class.java)
//        startActivity(intent)
//        finish()
    }
}