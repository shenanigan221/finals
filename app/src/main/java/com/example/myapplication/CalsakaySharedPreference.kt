package com.example.myapplication

import android.content.Context
import android.content.SharedPreferences

class CalsakaySharedPreference(val context: Context) {
    private val PREFS_NAME = "CALSAKAY_SHARED_PREFERNCES"
    val sharedPreferences: SharedPreferences = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

  fun save(KEY_NAME: String, text: String){
      val editor: SharedPreferences.Editor = sharedPreferences.edit()
      editor.putString(KEY_NAME, text)
      editor!!.commit()
  }

    fun getValueString(KEY_NAME: String): String? {
        return sharedPreferences.getString(KEY_NAME, null)
    }

    fun removeValue(KEY_NAME: String) {
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        editor.remove(KEY_NAME)
        editor.commit()
    }

    fun clearSharedPreference() {
        val editor: SharedPreferences.Editor = sharedPreferences.edit()
        //sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        editor.clear()
        editor.commit()
    }

}