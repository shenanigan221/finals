package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.Window
import android.view.WindowManager
import com.example.myapplication.driver.DriverHomeActivity
import com.example.myapplication.frontliner.FrontlinerHomeActivity

class MainActivity : AppCompatActivity() {
    lateinit var handler: Handler
    lateinit var calsakaySharedPreference: CalsakaySharedPreference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN)

        setContentView(R.layout.activity_main)

        calsakaySharedPreference = CalsakaySharedPreference(this)
        calsakaySharedPreference.save("URL", "http://192.168.1.18/calsakaywebapp/api/")

        handler = Handler()
        handler.postDelayed({
            if(calsakaySharedPreference.getValueString("hasUser") == "1"){
                if (calsakaySharedPreference.getValueString("role") == "1"){
                    val intent = Intent(this, FrontlinerHomeActivity::class.java)
                    startActivity(intent)
                    finish()
                }else if(calsakaySharedPreference.getValueString("role") == "2"){
                    val intent = Intent(this, DriverHomeActivity::class.java)
                    startActivity(intent)
                    finish()
                }else{
//                    val intent = Intent(this, LoginActivity::class.java)
//                    startActivity(intent)
//                    finish()
                }
            }else{
                    val intent = Intent(this, LoginActivity::class.java)
                    startActivity(intent)
                    finish()
            }

        }, 1000)
    }
}